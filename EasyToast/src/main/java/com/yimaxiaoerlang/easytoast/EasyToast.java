package com.yimaxiaoerlang.easytoast;

import android.content.Context;
import android.widget.Toast;

public class EasyToast {
    public static void easyToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}

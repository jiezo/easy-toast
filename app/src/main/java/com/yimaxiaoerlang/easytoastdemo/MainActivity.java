package com.yimaxiaoerlang.easytoastdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.yimaxiaoerlang.easytoast.EasyToast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EasyToast.easyToast(this, "你好");
    }
}